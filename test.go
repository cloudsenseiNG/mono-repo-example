package main

import (
    "net/http"
    "github.com/gorilla/csrf"
    "http/template"
)

func main() {
    r := http.NewServeMux()
    // ok: go-net-http-form-without-gorilla-csrf-protection
    r.HandleFunc("/signup", ShowSignupForm)
    // ruleid: go-net-http-form-without-gorilla-csrf-protection
    r.HandleFunc("/signup", ShowSignupForm2)

    // ok: go-net-http-form-without-gorilla-csrf-protection
    http.HandleFunc("/signup", ShowSignupForm)
    // ruleid: go-net-http-form-without-gorilla-csrf-protection
    http.HandleFunc("/signup", ShowSignupForm2)
}

func ShowSignupForm(w http.ResponseWriter, r *http.Request) {
    t.ExecuteTemplate(w, "signup_form.tmpl", map[string]interface{}{
        csrf.TemplateTag: csrf.TemplateField(r),
    })
}

func ShowSignupForm2(w http.ResponseWriter, r *http.Request) {
    t.ExecuteTemplate(w, "signup_form.tmpl", map[string]interface{}{
        "a": b,
    })
}
